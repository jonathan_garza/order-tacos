__author__ = 'jdg2874'

"""
Order torchy tacos
"https://mavn.is/torchys/ut/"
        # trailer_elem = driver.find_element_by_partial_link_text('Trailer Park')

        # trailer_elem = WebDriverWait(driver, 20).until(
        #     EC.presence_of_element_located((By.LINK_TEXT, 'Trailer Park'))
        # )
        # print driver.page_source

        # trailer_elem =  WebDriverWait(driver, 10).until\
        #     (lambda driver: driver.find_element_by_partial_link_text('Trailer Park'))

        # print "T Elem", trailer_elem.text
        # trailer_elem.click()

"""

import logging
import time
import yaml
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

f = open('order_info.yaml')
customer_order_info = yaml.load(f, Loader=yaml.loader.BaseLoader)
# print customer_order_info

def add_trailer(driver):
    trailer_elem = driver.find_element_by_partial_link_text('Trailer Park')
    trailer_elem.click()
    time.sleep(3)
    trash_elem = driver.find_element_by_id('product-mod-205')
    trash_elem.click()
    time.sleep(2)
    add_button = driver.find_element_by_id('button-cart-add')
    add_button.click()
    time.sleep(3)
    return driver

def add_democrat(driver):
    democrat_elem = driver.find_element_by_partial_link_text('The Democrat')
    democrat_elem.click()
    time.sleep(3)
    month_expire_select = Select(driver.find_element_by_xpath("//*[@data-parent_id='198']"))
    month_expire_select.select_by_visible_text("Corn")
    time.sleep(2)
    month_expire_select = Select(driver.find_element_by_xpath("//*[@data-parent_id='206']"))
    month_expire_select.select_by_visible_text("Diablo ****")
    time.sleep(2)
    add_button = driver.find_element_by_id('button-cart-add')
    add_button.click()
    return driver

def pay_process(driver):
    quantity_select = Select(driver.find_element_by_xpath("//*[@id='form-0']/select"))
    quantity_select.select_by_value(customer_order_info.get("taco_1_qty"))
    time.sleep(2)
    quantity_select = Select(driver.find_element_by_xpath("//*[@id='form-1']/select"))
    quantity_select.select_by_value(customer_order_info.get("taco_2_qty"))
    time.sleep(2)

    submit_button = driver.find_element_by_id('pay-button')
    submit_button.click()

    name_elem = driver.find_element_by_id('id_customer_name')
    name_elem.send_keys(customer_order_info.get("customer_name"))
    time.sleep(1)
    email_elem = driver.find_element_by_id('id_email')
    email_elem.send_keys(customer_order_info.get("customer_email"))
    time.sleep(1)
    phone_elem = driver.find_element_by_id('id_phone')
    phone_elem.send_keys(customer_order_info.get("phone_number"))
    time.sleep(1)

    if customer_order_info.get("pay_online"):
        pay_online_elem = driver.find_element_by_id('id_pay_online')
        pay_online_elem.click()
        time.sleep(1)
        credit_num_elem = driver.find_element_by_id('id_credit_number')
        credit_num_elem.send_keys(customer_order_info.get("cc_number"))
        time.sleep(2)
        month_expire_select = Select(driver.find_element_by_id('id_month_expires'))
        month_expire_select.select_by_value(customer_order_info.get("cc_month_expire"))
        time.sleep(2)
        year_expire_select = Select(driver.find_element_by_id('id_year_expires'))
        year_expire_select.select_by_value(customer_order_info.get("cc_year_expire"))
        time.sleep(2)
        ccv_elem = driver.find_element_by_id('id_ccv')
        ccv_elem.send_keys(customer_order_info.get("cc_ccv"))
        time.sleep(2)

    if customer_order_info.get("finalize"):
        print "Completing the order"
        driver.save_screenshot('taco_order.png')

        # finalize_button = driver.find_element_by_id('finalize-button')
        # finalize_button.click()

def main():
    print "Ordering your Favorite Tacos..."
    try:
        driver = webdriver.Firefox()
        driver.maximize_window()
        driver.implicitly_wait(20)
        driver.get("https://mavn.is/torchys/ut/")
        #Check that cart is empty
        # cart_elem = WebDriverWait(driver, 20).until(
        #     EC.presence_of_element_located((By.ID, 'cart-toggle'))
        # )

        # taco_toggle = driver.find_element_by_xpath("//*[@id='menu-item-list']/section[4]")
        # taco_text = taco_toggle.find_element_by_class_name('inner')
        # ActionChains(driver).move_to_element(taco_toggle).click(taco_text).perform()
        print "Click Taco button"
        time.sleep(3)
        taco_toggle = driver.find_element_by_xpath('//*[@id="menu-item-list"]/section[4]/header/div[2]')

        taco_toggle.click()
        print "Add Trailer taco. Extra Trashy"
        time.sleep(3)
        add_trailer(driver)
        print "Add Democrat taco. Corn and Diablo Sauce"
        add_democrat(driver)
        time.sleep(3)

        cart_elem = driver.find_element_by_id('cart-toggle')
        cart_elem = WebDriverWait(driver, 20).until(
            lambda driver: driver.find_element_by_id('cart-toggle')
        )
        print "I have {0} tacos.".format(cart_elem.text)
        print "Add more tacos"
        cart_content = driver.find_element_by_id('cart-content')
        cart_open = cart_content.is_displayed()

        if not cart_open:
            cart_elem = driver.find_element_by_id('cart-toggle')
            cart_elem.click()

        pay_process(driver)

        time.sleep(5)

    except Exception, e:
        print "Exception Message", e

    finally:
        logging.info("Closing driver...")
        driver.close()
        driver.quit()

if __name__ == '__main__':
    main()